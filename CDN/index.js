require('dotenv').config();
const express = require('express');
const app = express();
const mime = require('mime-to-extensions');
const multer = require('multer');

const PORT = process.env.PORT || 3000;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${mime.extension(file.mimetype)}`)
  }
});
const upload = multer({ storage: storage });

app.use('/uploads', express.static('uploads'));

app.get('/', function (req, res) {
  res.send('200');
});

app.post('/uploads', function (req, res, next) {
  upload.single('image')(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(406).json({
        message: 'Dữ liệu không hợp lệ',
      });
    } else if (err) {
      return res.status(406).json({
        message: 'Có lỗi khi tải lên',
      });
    }

    if (!req.file) {
      return res.status(406).json({
        message: 'Dữ liệu không hợp lệ',
      });
    }

    res.status(200).json({
      filename: req.file.filename,
      path: req.file.path,
      url: `${process.env.HOSTNAME}/${req.file.path}`,
    });
  })
})

app.listen(PORT);
console.log('server running at', PORT);