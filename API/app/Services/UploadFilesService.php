<?php

namespace App\Services;


use App\Entities\Repositories\UploadFilesRepository;
use Illuminate\Http\Request;

class UploadFilesService extends BaseService {
    protected $base_api;

    public function __construct(UploadFilesRepository $categoryRepository) {
        $this->repository = $categoryRepository;
        $this->base_api = 'cdn-storage:3000/uploads';
    }

    public function store(Request $request) {
        $requestArray = $request->toArray();
        try {
            if ($request->hasFile('image')) {
                $ch = curl_init($this->base_api);
                curl_setopt_array($ch, array(
                    CURLOPT_POST => 1,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POSTFIELDS => array(
                        'image' => curl_file_create($request->file('image')->getRealPath(), $request->file('image')->getMimeType(), $request->file('image')->getClientOriginalName())
                    )
                ));
                $response = curl_exec($ch);

                if (!empty($response)) {
                    $data = json_decode($response);
                    $requestArray['name'] = $data->filename;
                    $requestArray['url'] = $data->url;
                    return $this->repository->create($requestArray);
                }
            }
            return [];
        } catch (\Error $error) {
            return $error->getMessage();
        }
    }
}
