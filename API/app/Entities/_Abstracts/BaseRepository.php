<?php

namespace App\Entities\_Abstracts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as Application;
use Illuminate\Support\Facades\Schema;

abstract class BaseRepository
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = null;

    /**
     * Collection of Criteria
     *
     * @var Collection
     */
    protected $criteria;

    /**
     * An amount of paginated results for fetch queries by this repository.
     *
     * @var integer
     */
    protected $paginateCount;
    /**
     * Set if create / update methods should add created_by / updated_by fields
     *
     * @var boolean
     */
    protected $addCreatorAndUpdater = true;
    /**
     * Array of attributes that can be returned with the magic __get method
     * @var array
     */
    protected $allowedAttributes = [];

    protected $all_columns;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->criteria = new Collection();
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    abstract public function model();

    public function makeModel()
    {
        $model = $this->app->make($this->model());
        return $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function pagination($page = 20)
    {
        return $this->model->paginate($page);
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    /**
     * Insert data or multi data
     *
     * @param array $attributes
     * @return mixed
     */
    public function insert(array $attributes)
    {
        return $this->model->insert($attributes);
    }

    /**
     * Creates a new entry in the model.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $relations = [];
        foreach ($data as $key => $value) {
            if (is_array($value) && method_exists($this->model, $key)) {
                $relations[$key] = $value;
                unset($data[$key]);
            }
        }
        $instance = $this->model->create($data);
        // reload model - this hac need to force refreshing primary key
        // when primary key is not increment and it will be 0 without reloading model
        // TODO: make it better
        if (array_key_exists($instance->getKeyName(), $data)) {
            $instance = $this->getById($data[$instance->getKeyName()]);
        }
        // add relation data
        foreach ($relations as $relationName => $relationData) {
            $instance->$relationName()->sync($relationData);
        }
        return $instance;
    }

    /**
     * Updates an existing model.
     *
     * @param  int $id
     * @param  array $data
     *
     * @return Model
     */
    public function update(array $data, $id)
    {
        // load instance
        $instance = $this->model->where($this->model->getKeyName(), '=', $id)->first();
        // collect relation data into separate array
        $relations = [];
        foreach ($data as $key => $value) {
            if (is_array($value) && method_exists($this->model, $key)) {
                $relations[$key] = $value;
                unset($data[$key]);
            }
        }
        // update model
        $instance->update($data);

        // update relation data
        foreach ($relations as $relationName => $relationData) {
            $instance->$relationName()->sync($relationData);
        }
        return $instance;
    }

    /**
     * Removes an entry from the model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->where($this->model->getKeyName(), '=', $id)->delete();
    }


    /**
     * Applies the given where conditions to the model.
     *
     * @param array $where
     * @return void
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $this->model = $this->model->where($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    public function whereBuilder($conditions, $builder)
    {
        if ($conditions instanceof Collection) {
            $conditions = $conditions->filter(function ($value) {
                return (empty($value) && $value !== '0') ? false : true;
            })->all();
        }

        foreach ($conditions as $field => $value) {
            if (is_array($value) && !empty($value) && count($value) > 2) {
                list($field, $condition, $val) = $value;
                if (!$this->isFillable($field)) {
                    continue;
                }
                $builder = $builder->where($field, $condition, $val);
            } else {
                if (!$this->isFillable($field)) {
                    continue;
                }

                $builder = $builder->where($field, '=', $value);
            }
        }

        return $builder;
    }

    public function orderBuilder($sorts, $builder)
    {
        $fillables = $this->getFillable();

        if (empty($sorts) && in_array('created_at', $fillables)) {
            if (Schema::hasColumn($this->model->getTableName(), 'id')) {
                $sorts = ['id' => 'ASC'];
            }
        }

        if (isset($sorts) and !empty($sorts)) {
            foreach ($sorts as $k => $v) {
                if (!in_array($k, $fillables)) {
                    continue;
                }
                $builder = $builder->orderBy($k, $v);
            }
        }

        return $builder;
    }

    public function getFillable()
    {
        $fillables = $this->model->getFillable();
        $primaryKey = $this->model->getKeyName();
        $createdAt = $this->model->getCreatedAtColumn();
        $updatedAt = $this->model->getUpdatedAtColumn();
        if (!in_array($primaryKey, $fillables)) {
            array_push($fillables, $primaryKey, $createdAt, $updatedAt);
        }

        $this->all_columns = $fillables;

        return $fillables;
    }

    public function isFillable($key)
    {
        if (!$this->all_columns) {
            $this->getFillable();
        }

        if (!in_array($key, $this->all_columns)) {
            return false;
        }

        return true;
    }
}
