<?php

namespace App\Entities\Admin\Models\_Abstract;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Entities\Admin\Models\_Abstract
 */
abstract class BaseModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }
}
