<?php

namespace App\Entities\Repositories;

use App\Entities\_Abstracts\BaseRepository;
use App\Entities\Models\FilesModel;

class UploadFilesRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FilesModel::class;
    }
}
