<?php

namespace App\Entities\Traits;

trait ApiResponseTrait
{
    /**
     * API Success Response
     *
     * @param $message
     * @param $data
     * @param array $meta
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function successResponse($data = null, $message = 'Request success', $meta = [])
    {
        return response()->json([
            'status' => 'OK',
            'message' => $message,
            'meta' => $meta,
            'data' => $data
        ], 200);
    }

    /**
     * API Fail Response
     *
     * @param $message
     * @param $errors
     * @param int $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function failResponse($errors = null, $message = 'Something went wrong, please check again !!!', $statusCode = 400)
    {
        return response()->json([
            'status' => 'Error',
            'message' => $message,
            'errors' => $errors
        ], $statusCode);
    }
}
