<?php

namespace App\Entities\Models;

use App\Entities\Admin\Models\_Abstract\BaseModel;

class FilesModel extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'upload_files';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url'
    ];

    protected $timestamp = true;
}
