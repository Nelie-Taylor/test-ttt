<?php

namespace App\Http\Controllers;

use App\Services\UploadFilesService;
use Illuminate\Http\Request;

class UploadFilesController extends BaseController
{

    protected $uploadFilesService;

    public function __construct(UploadFilesService $uploadFilesService)
    {
        $this->uploadFilesService = $uploadFilesService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = $this->uploadFilesService->search();
        return $this->successResponse($categories,'List files');
    }

    /**
     *
     * Store the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required'
        ]);

        $data = $this->uploadFilesService->store($request);
        return $this->successResponse($data, 'Stored data successfully');
    }
}
