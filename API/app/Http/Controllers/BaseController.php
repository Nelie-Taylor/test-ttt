<?php

namespace App\Http\Controllers;

use App\Entities\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    use ApiResponseTrait;

    /**
     * @param $result
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, $message)
    {
        if (!empty($result)) {
            $response = [
                'status' => true,
                'data' => $result,
                'message' => $message,
            ];
        } else {
            $response = [

                'status' => true,
                'message' => $message,
            ];
        }
        return response()->json($response, 200);
    }

    /**
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'status' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
